var tournamentTree = (function () {
    var bigData = {
        teams: [],
        results: []
    },
        shuffle = function (a) {
            var j, x, i;
            for (i = a.length - 1; i > 0; i--) {
                j = Math.floor(Math.random() * (i + 1));
                x = a[i];
                a[i] = a[j];
                a[j] = x;
            }
        };

    return {
        bigData : bigData,
        generateTree: function () {
            shuffle(playerList.userData.players);
            tournamentTree.populateTree();
            $('#tree').bracket({ init: bigData, skipConsolationRound:  true });
        },
        populateTree: function () {
            for (i = 0; i < playerList.userData.players.length; i = i + 2) {
                bigData.teams.push([playerList.userData.players[i], playerList.userData.players[i + 1]]);
            }
        },
        updateTree: function () {
            $('#tree').html();
            $('#tree').bracket({ init: bigData, skipConsolationRound:  true });
        }
    }


})(),
playerList = (function () {
    var userData = {
        players: [],
        login: ""
    },
    onlyUnique = function (value, index, self) {
        return self.indexOf(value) === index;
    };

    return {
        userData: userData,
        validatePlayers: function () {
            if (Math.log2(userData.players.length) % 1 === 0 && userData.players.length >= 4) {
                document.getElementById("start").disabled = false;
            } else {
                document.getElementById("start").disabled = true;
            }
        },
        validateInput: function () {
            if (document.getElementById("newPlayer").value == "") {
                alert("You cannot give an empty login. Try again.")
            } else {
                userData.login = document.getElementById("newPlayer").value;
                document.getElementById("newPlayer").value = "";
                userData.players.push(userData.login);
                userData.players = userData.players.filter(onlyUnique);
            }
        },
        showUserListOnSite: function () {
            var list = "";
            for (var i = 0; i < userData.players.length; i++) {
                list = list + userData.players[i] + "<br/>";

            }
            return document.getElementById("userList").innerHTML = list;
        },
        getPlayerList: function () {
            playerList.validateInput();
            playerList.showUserListOnSite();
            playerList.validatePlayers();
            return userData.players;
        }
    }
})();





