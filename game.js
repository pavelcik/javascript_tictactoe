var game = (function () {
    var player1 = {
        image: "<img src='images/o.png' title='O'>",
        figure: 'O'
    },
        player2 = {
            image: "<img src='images/x3.png' title='X'>",
            figure: 'X'
        },
        shouldAdvanceRound = function () {
            return roundNumber != playerList.userData.players.length;
        },
        firstPlayerMove = true,
        whosPlaying = document.getElementById("whosPlaying"),
        nowPlaying = document.getElementById("NowPlaying"),
        NONE = "",
        board = [NONE, NONE, NONE,
            NONE, NONE, NONE,
            NONE, NONE, NONE],
        gameStarted = false,
        counter = 0,
        winners = [],
        round = [],
        roundNumber;

    return {
        board : board,
        player1 : player1,
        player2 : player2,
        startGame: function () {
            document.getElementById("start").disabled = false;
            gameStarted = true;
            roundNumber = playerList.userData.players.length;
            tournamentTree.generateTree();
            game.showNowPlaying();
        },
        calculateMove: function (square) {
            if (board[square.id] == NONE && gameStarted) {
                whosPlaying.innerText = playerList.userData.players[counter] + "'s turn";
                if (firstPlayerMove) {
                    board[square.id] = player1.figure;
                    game.drawFirstPlayerFigure(square);
                    game.checkForPlayer1Win();

                } else {
                    board[square.id] = player2.figure;
                    game.drawSecondPlayerFigure(square);
                    game.checkForPlayer2Win();
                }
                firstPlayerMove = !firstPlayerMove;
                winnerCheck.checkForDraw(square);
                    
            }
        },
        drawFirstPlayerFigure: function (square) {
            return square.innerHTML = player1.image;
        },
        drawSecondPlayerFigure: function (square) {
            return square.innerHTML = player2.image;
        },
        checkForPlayer1Win: function () {
            if (winnerCheck.checkForWinner(player1.figure)) {
                alert(playerList.userData.players[counter] + " won!");
                winners.push(playerList.userData.players[counter]);
                game.checkCounter();
                firstPlayerMove = false;
                round.push([1, 0]);
                game.resetBoard();
                tournamentTree.updateTree();
                game.showNowPlaying();
                whosPlaying.innerText = playerList.userData.players[counter] + "'s turn";
            }
            else whosPlaying.innerText = playerList.userData.players[counter + 1] + "'s turn";
        },

        checkForPlayer2Win: function () {
            if (winnerCheck.checkForWinner(player2.figure)) {
                alert(playerList.userData.players[counter+1] + " won!");
                winners.push(playerList.userData.players[counter + 1]);
                game.checkCounter();
                firstPlayerMove = false;
                round.push([0, 1]);
                game.resetBoard();
                tournamentTree.updateTree();
                game.showNowPlaying();
                whosPlaying.innerText = playerList.userData.players[counter] + "'s turn";
            }
        },
        resetBoard: function () {
            for (i = 0; i < 9; i++) {
                board[i] = NONE;
                if (document.getElementById(i) != null) {
                    document.getElementById(i).innerHTML = "";
                }
            }

            if (shouldAdvanceRound()) {
                tournamentTree.bigData.results.push(round);
                round = [];
                roundNumber = roundNumber / 2;
            }

        },
        showNowPlaying: function () {
            nowPlaying.hidden = false;
            if (playerList.userData.players.length == 1) {
                alert(playerList.userData.players[counter] + " won the competition!");
                game.endGame();
            } else {
                nowPlaying.innerText = "Now Playing: \n" + playerList.userData.players[counter] + "\n" + playerList.userData.players[counter + 1];
            }
        },
        showNowPlayingWhenDraw: function() {
            firstPlayerMove=true;
           return whosPlaying.innerText = playerList.userData.players[counter] + "'s turn";
        },
        checkCounter: function () {
            if (counter < playerList.userData.players.length - 2) {
                counter = counter + 2;
            } else {
                playerList.userData.players = winners.slice();
                winners = [];
                counter = 0;

            }
        },
        endGame: function () {
            gameStarted = false;
        },
        getBoard: function () {
            return board;
        }
    }

})();