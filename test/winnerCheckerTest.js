describe('winnerCheckerTest', function () {
    it('shouldSayIAmAWinnerWithO', function () {
        game.board = ["O", "", "", "O", "X", "", "O", "", "X"],
            figure = "O";
        var result = winnerCheck.checkForWinner(figure);
        expect(result).toBe(true);
    });
    it('shouldSayIAmAWinnerWithX', function () {
        game.board = ["X", "", "", "X", "O", "", "X", "", "O"],
            figure = "X";
        var result = winnerCheck.checkForWinner(figure);
        expect(result).toBe(true);
    });

    it('shouldNotSayIAmAWinner', function () {
        game.board = ["O", "", "", "X", "X", "", "O", "", "X"],
            figure = "O";
        var result = winnerCheck.checkForWinner(figure);
        expect(result).toBe(false);
    });

    it('shoulCheckIfAllBoardIsOccupiedForADraw', function () {
        var square = 'td#0.Square';
        game.board = ["O", "O", "X", "X", "O", "O", "O", "X", "X"];

        var result = game.board.every(allBoardOccupied)
        expect(result).toBe(true);
    });
    it('shoulNotCheckIfAllBoardIsOccupiedForADraw', function () {

        var square = 'td#0.Square';
        game.board = ["O", "O", "X", "X", "O", "", "O", "X", "X"];

        var result = game.board.every(allBoardOccupied)

        expect(result).toBe(false);
    });
    it('shouldNotGiveAlertThatBoardIsOccupied', function () {

        var square = 'td#0.Square';
        game.board = ["O", "O", "X", "X", "O", "", "O", "X", "X"];


        spyOn(window, 'alert').and.returnValue("It's a draw! Try again.");
        winnerCheck.checkForDraw();
        expect(window.alert).not.toHaveBeenCalledWith("It's a draw! Try again.");
    });
    it('shouldNotGiveAlertThatBoardIsOccupied', function () {

        var square = 'td#0.Square';
        game.board = ["O", "O", "X", "X", "O", "O", "O", "X", "X"];


        spyOn(window, 'alert').and.returnValue("It's a draw! Try again.");
        winnerCheck.checkForDraw();
        expect(window.alert).toHaveBeenCalledWith("It's a draw! Try again.");
    });
});



