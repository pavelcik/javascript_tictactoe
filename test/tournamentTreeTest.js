describe('tournamentTreeTest', function () {

   
    it('shouldCheckIfUserTreeIsPopulated', function () {
        playerList.userData.players = ["a", "b", "c", "d"],
            tournamentTree.bigData.teams = [];
        var properLength = 2;
        tournamentTree.populateTree();
        expect(tournamentTree.bigData.teams.length).toBe(properLength);
    });
    it('shouldCheckIfGameIsPossibleWithRightAmountOfPlayers', function () {

        playerList.userData.players = ["a", "b", "c", "d"],
            tournamentTree.bigData.teams = [];
        var properLength = 2;
        var dummyElement = document.createElement('div');
        document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);



        playerList.validatePlayers();
        expect(dummyElement.disabled).toBe(false);

    });
    it('shouldCheckIfGameIsPossibleWithWrongAmountOfPlayers', function () {

        playerList.userData.players = ["a", "b", "c"];
        var dummyElement = document.createElement('div');
        document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
        playerList.validatePlayers();
        expect(dummyElement.disabled).toBe(true);

    });
    it('shouldCheckIfGameIsPossibleWithWrongAmountOfPlayersBecauseThereIsLessThanFourPeoplePlaying', function () {

        playerList.userData.players = ["a", "b"];
        var dummyElement = document.createElement('div');
        document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
        playerList.validatePlayers();
        expect(dummyElement.disabled).toBe(true);

    });
    it('alertShouldSayThatYouCannotGiveAnEmptyLogin', function () {
        playerList.userData.players = ["a", "b"];
        var dummyElement = document.createElement('div');
        document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement),
        dummyElement.value="";

        spyOn(window, 'alert').and.returnValue("You cannot give an empty login. Try again.");
        playerList.validateInput();

        expect(window.alert).toHaveBeenCalledWith("You cannot give an empty login. Try again.");

    });
    it('alertShouldNotSayThatYouCannotGiveAnEmptyLogin', function () {
        playerList.userData.players = ["a", "b"];
        var dummyElement = document.createElement('div');
        document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement),
        dummyElement.value="a";

        spyOn(window, 'alert').and.returnValue("You cannot give an empty login. Try again.");
        playerList.validateInput();

        expect(window.alert).not.toHaveBeenCalledWith("You cannot give an empty login. Try again.");

    });
    it('shouldClearLoginFormAfterSubmittingIt', function () {
        playerList.userData.players = ["a", "b","c","d"];
        var dummyElement = document.createElement('div');
        document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
        dummyElement.value="a";
        playerList.validateInput();

        expect(dummyElement.value).toBe("");

    });
    it('shouldCheckPlayersLength', function () {
       
        var dummyElement = document.createElement('div');
        document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
        dummyElement.value="a";
        playerList.userData.players = [];
        playerList.validateInput();

        expect(playerList.userData.players.length).toBe(1);

    });
    it('shouldCheckPlayersLengthWhenAddingNothing', function () {
        
         var dummyElement = document.createElement('div');
         document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
         dummyElement.value="";
         playerList.userData.players = [];
         playerList.validateInput();
 
         expect(playerList.userData.players.length).toBe(0);
 
     });
     it('shouldShowUserListOnSite', function () {
        
         var dummyElement = document.createElement('div');
         document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
         dummyElement.value="";
         playerList.userData.players = [],
         list="";
         playerList.showUserListOnSite();
 
         expect(dummyElement.innerHTML).toBe(list);
 
     });  
     it('shouldCheckIfUserIsAddedToTheListOnSite', function () {
        
         var dummyElement = document.createElement('div');
         document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
         dummyElement.value="";
         playerList.userData.players = ["a"],
         list="";
         playerList.showUserListOnSite();
         expect(dummyElement.innerHTML).toBe("a<br>");
 
     });    
     it('shouldNotAddOtherPlayerBecauseOfOnlyUniqueNames  ', function () {
        
         var dummyElement = document.createElement('div');
         document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
         dummyElement.value="a";
         playerList.userData.players = ["a"],
         playerList.validateInput();
         expect(playerList.userData.players.length).toBe(1);
 
     });    
     it('shouldAddNewPlayer', function () {
        
         var dummyElement = document.createElement('div');
         document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
         dummyElement.value="b";
         playerList.userData.players = ["a"],
         playerList.validateInput();
         expect(playerList.userData.players.length).toBe(2);
 
     });   
});