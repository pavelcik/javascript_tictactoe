describe('tournamentTreeTest', function () {
    
       
        it('shouldDrawPlayer1Figure', function () {
            playerList.userData.players = ["a", "b", "c", "d"],
            game.counter=0,
            game.firstPlayerMove=true,
            game.gameStarted=true,
            player1 = {
                figure: "O",
                image: '<img src="images/o.png" title="O">'
            }
            board=[];
            var dummyElement = document.createElement('div');
            
            document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
            dummyElement.setAttribute("id","3");

            game.drawFirstPlayerFigure(dummyElement);
            expect(dummyElement.innerHTML).toBe(player1.image);
        });
        it('shouldDrawPlayer2Figure', function () {
            playerList.userData.players = ["a", "b", "c", "d"],
            game.counter=0,
            game.firstPlayerMove=true,
            game.gameStarted=true,
            player2 = {
                figure: "O",
                image: '<img src="images/x3.png" title="X">'
            }
            board=[];
            var dummyElement = document.createElement('div');
            
            document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
            dummyElement.setAttribute("id","3");

            game.drawSecondPlayerFigure(dummyElement);
            expect(dummyElement.innerHTML).toBe(player2.image);
        });
        it('shouldResetBoad', function () {
            player2 = {
                figure: "O",
                image: '<img src="images/x3.png" title="X">'
            }
            game.board = ["O", "", "", "O", "X", "", "O", "", "X"];
            var dummyElement = document.createElement('div');
            
            document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
            dummyElement.setAttribute("id","3");
            dummyElement.innerHTML=player2.image;

            game.resetBoard();
            expect(dummyElement.innerHTML).toBe('');
        });
    });