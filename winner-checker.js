var winnerCheck = (function() {
    allBoardOccupied = function(element) {
        return (element==game.player1.figure||element==game.player2.figure);       
    };
    return {
        checkForWinner: function(figure) {
            var result = false;
            if (winnerCheck.checkRow(0, 1, 2, figure) ||
            winnerCheck.checkRow(3, 4, 5, figure) ||
            winnerCheck.checkRow(6, 7, 8, figure) ||
            winnerCheck.checkRow(0, 3, 6, figure) ||
            winnerCheck.checkRow(1, 4, 7, figure) ||
            winnerCheck.checkRow(2, 5, 8, figure) ||
            winnerCheck.checkRow(0, 4, 8, figure) ||
            winnerCheck.checkRow(2, 4, 6, figure)) {
            result = true;
        }
        return result;
        },
        checkRow: function(a,b,c,figure) {
            var result = false;
            if (game.board[a] == figure && game.board[b] == figure && game.board[c] == figure) {
                result = true;
            }
            return result;
        },
        
        checkForDraw: function(square) {
            if(game.board.every(allBoardOccupied)) {
                alert("It's a draw! Try again.");
                game.resetBoard();
                game.showNowPlayingWhenDraw();
                
                
            } 
        }

    }
})();